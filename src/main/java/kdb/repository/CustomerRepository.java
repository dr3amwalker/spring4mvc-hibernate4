package kdb.repository;

import kdb.domain.Customer;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public class CustomerRepository implements ICustomerRepository {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public List<Customer> getCustomers(){
        return sessionFactory.getCurrentSession().createCriteria(Customer.class).list();
    }

    @Override
    public void addCustomer(Customer customer){
        sessionFactory.getCurrentSession().save(customer);
    }

    @Override
    public void deleteCustomer(Customer customer) {
        sessionFactory.getCurrentSession().delete(customer);
    }

    @Override
    public void editCustomer(Customer customer) {
        sessionFactory.getCurrentSession().merge(customer);
    }

    @Override
    public Customer getCustomerById(int id) {
        return (Customer)sessionFactory.getCurrentSession()
                .createCriteria(Customer.class)
                .add(Restrictions.eq("id", id))
                .uniqueResult();
    }

}
