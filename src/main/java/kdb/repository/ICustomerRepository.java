package kdb.repository;

import kdb.domain.Customer;

import java.util.List;

/**
 * Created by Alex on 8/31/2014.
 */
public interface ICustomerRepository {

    public List<Customer> getCustomers();

    public void addCustomer(Customer customer);

    public void deleteCustomer(Customer customer);

    public void editCustomer(Customer customer);

    public Customer getCustomerById(int id);

}
