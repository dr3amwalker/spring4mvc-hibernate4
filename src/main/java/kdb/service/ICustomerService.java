package kdb.service;

import kdb.domain.Customer;

import java.util.List;

public interface ICustomerService {

    public List<Customer> getCustomers();

    public void addCustomer(Customer customer);

    public void deleteCustomer(Customer customer);

    public void editCustomer(Customer customer);

    public Customer getCustomerById(int id);
}
